package org.example;

import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {


    ProductService productService;
    ProductRepository productRepository;


    @BeforeEach
    void setup() {
        productRepository = Mockito.mock(ProductRepository.class);
        productService = new ProductService(productRepository);

    }

    @Test
    void test_get_products() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        var product1 = new Product(2, "hej", "hejhej", "image", 100);
        var product2 = new Product(3, "hej", "hejhej", "image", 100);
        Mockito.when(productRepository.getProducts()).thenReturn(List.of(product, product1, product2));

        var result = productService.getProducts();

        Assertions.assertTrue(result.containsAll(List.of(product, product1, product2)));

    }

    @Test
    void test_get_one_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);

        Mockito.when(productRepository.getProduct(product.getId())).thenReturn(Optional.of(product));

        Assertions.assertDoesNotThrow(() -> productService.getProduct(product.getId()));

        Mockito.verify(productRepository).getProduct(product.getId());
    }

    @Test
    void test_add_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        Assertions.assertDoesNotThrow(() -> {
            productService.addProduct(product);
        });
        Mockito.verify(productRepository).save(product);
    }

    @Test
    void test_delete_one_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        Mockito.when(productRepository.getProduct(product.getId())).thenReturn(Optional.of(product));

        Assertions.assertDoesNotThrow(() -> {
            productService.delete(product.getId());
        });

        Mockito.verify(productRepository).delete(product.getId());
    }

    @Test
    void test_edit_product() {
        var updatedProduct = new Product(1, "hejdå", "hejdååå", "image1", 200);
        Assertions.assertDoesNotThrow(() -> productService.editProduct(updatedProduct));

        Mockito.verify(productRepository).editProduct(updatedProduct);
    }
}