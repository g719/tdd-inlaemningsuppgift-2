package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryProductRepositoryTest {

    ProductRepository productRepository;

    @BeforeEach
    void setup() {
        productRepository = new InMemoryProductRepository();
    }

    @Test
    void test_get_products() {
       var result = productRepository.getProducts();
        Assertions.assertEquals(0,result.size());
    }


    @Test
    void test_add_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        Assertions.assertDoesNotThrow(() -> productRepository.save(product));
        Assertions.assertEquals(1, productRepository.getProducts().size());
    }

    @Test
    void test_get_one_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        Assertions.assertDoesNotThrow(() -> productRepository.save(product));
        var result = productRepository.getProduct(product.getId()).orElseThrow().getId();
        Assertions.assertEquals(1 , result);
    }

    @Test
    void test_delete_one_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        Assertions.assertDoesNotThrow(() -> productRepository.save(product));
        productRepository.delete(product.getId());
        Assertions.assertEquals(0, productRepository.getProducts().size());
    }


    @Test
    void test_edit_one_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        Assertions.assertDoesNotThrow(() -> productRepository.save(product));

        var updatedProduct = new Product(1, "hejdå", "hejhejdå", "image1", 200);
        Assertions.assertDoesNotThrow(() -> productRepository.editProduct(updatedProduct));
        var result = productRepository.getProduct(updatedProduct.getId()).orElseThrow();

        Assertions.assertEquals(updatedProduct.getTitle(), result.getTitle());
        Assertions.assertEquals(updatedProduct.getDescription(), result.getDescription());
        Assertions.assertEquals(updatedProduct.getImage(), result.getImage());
        Assertions.assertEquals(updatedProduct.getPrice(), result.getPrice());


    }
}