package org.example;

import com.google.gson.Gson;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.HttpStatus;
import io.javalin.testtools.JavalinTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ProductControllerTest {

    @Test
    void test_controller_get_products() {

        var repository = new InMemoryProductRepository();
        var service = new ProductService(repository);
        var controller = new ProductController(service);
        var app = Main.createApp(controller);

        JavalinTest.test(app, (server, client) -> {
            var code = client.get("/").code();
            var body = client.get("/").body().string();

            Assertions.assertEquals(200, code);
            Assertions.assertEquals("[]", body);
        });
    }

    @Test
    void test_controller_get_one_product() {

        var product = new Product(1, "hej", "hejhej", "image", 100);
        var gson = new Gson();
        var output = gson.toJson(product);
        var repository = Mockito.mock(ProductRepository.class);
        Mockito.when(repository.getProduct(product.getId())).thenReturn(Optional.of(product));

        var service = new ProductService(repository);
        var controller = new ProductController(service);
        var app = Main.createApp(controller);

        JavalinTest.test(app, (server, client) -> {
            var response = client.get("/1");
            var code = response.code();
            var body = Objects.requireNonNull(response.body()).string();

            Assertions.assertEquals(200, code);
            Assertions.assertEquals(output, body);

        });
    }

    @Test
    void test_controller_add_product() {
        var repository = new InMemoryProductRepository();
        var service = new ProductService(repository);
        var controller = new ProductController(service);
        var app = Main.createApp(controller);

        JavalinTest.test(app, (server, client) -> {

            var gson = new Gson();
            var product = new Product(1, "hej", "hejhej", "image", 100);
            var output = gson.toJson(product);

            var response = client.post("/", product);
            var code = response.code();
            var body = response.body();

            Assertions.assertEquals(200, code);
            Assertions.assertEquals(output, body.string());
        });
    }

    @Test
    void test_controller_delete_product() {
        var product = new Product(1, "hej", "hejhej", "image", 100);
        var repository = Mockito.mock(ProductRepository.class);
        Mockito.when(repository.getProduct(product.getId())).thenReturn(Optional.of(product));

        var service = new ProductService(repository);
        var controller = new ProductController(service);
        var app = Main.createApp(controller);

        JavalinTest.test(app, (server, client) -> {
            var response = client.delete("/1");
            var code = response.code();

            Assertions.assertEquals(200, code);

            Mockito.verify(repository).delete(product.getId());

        });
    }

    @Test
    void test_controller_edit_product() {
        var repository = new InMemoryProductRepository();
        var service = new ProductService(repository);
        var controller = new ProductController(service);
        var app = Main.createApp(controller);

        JavalinTest.test(app, (server, client) -> {

            var product = new Product(1, "hej", "hejhej", "image", 100);
            repository.save(product);

            var updatedProduct = new Product(1, "hejhej", "hejhejdå", "image1", 200);

            var gson = new Gson();
            var output = gson.toJson(updatedProduct);

            var response = client.put("/", updatedProduct);
            var code = response.code();
            var body = response.body();

            Assertions.assertEquals(200, code);
            Assertions.assertEquals(output, body.string());
        });
    }
}