package org.example;

import java.util.Collection;
import java.util.Optional;

public interface ProductRepository {

    Collection<Product> getProducts();
    Optional<Product> getProduct(int id);

    Optional<Product> editProduct(Product product);

    void save(Product product);

    void delete(int id);

}
