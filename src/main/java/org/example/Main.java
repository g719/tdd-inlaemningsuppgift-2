package org.example;

import io.javalin.Javalin;

public class Main {

    public static void main(String[] args) {
        var repository = new InMemoryProductRepository();
        var service = new ProductService(repository);
        var controller = new ProductController(service);

        createApp(controller).start(8081);
    }

    public static Javalin createApp(ProductController controller) {
        Javalin app = Javalin.create()
                .get("/", controller::getProducts)
                .get("/{id}", controller::getProduct)
                //.get("/cart", getCart)
                //.post("/cart", addToCart)
                .put("/", controller::editProduct)
                .delete("/{id}", controller::deleteProduct)
                .post("/", controller::addProduct);
                //.delete("/cart", deleteCartItem)
                //.put("/cart", removeItemFromCart);*/

        return app;
    }
}