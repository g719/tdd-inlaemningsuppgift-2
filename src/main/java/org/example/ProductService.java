package org.example;

import java.util.Collection;
import java.util.Optional;

public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Collection<Product> getProducts() {
        return this.productRepository.getProducts();
    }

    public Optional<Product> getProduct(int id) {
        return productRepository.getProduct(id);
    }

    public Product addProduct(Product product) {
        productRepository.save(product);
        return product;
    }

    public void delete(int id) {
        productRepository.delete(id);
    }

    public Optional<Product> editProduct(Product product) {
        return productRepository.editProduct(product);
    }


}
