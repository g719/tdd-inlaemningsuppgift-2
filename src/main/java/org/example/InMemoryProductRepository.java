package org.example;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryProductRepository implements ProductRepository {

    private final Map<Integer, Product> products = new HashMap<>();
    @Override
    public Collection<Product> getProducts() {
        return products.values();
    }

    @Override
    public Optional<Product> getProduct(int id) {
        var value = products.get(id);
        return Optional.ofNullable(value);
    }

    @Override
    public Optional<Product> editProduct(Product product) {
        products.replace(product.getId(), product);
        return Optional.of(product);
    }

    @Override
    public void save(Product product) {
        products.put(product.getId(), product);
    }

    @Override
    public void delete(int id) {
        products.remove(id);
    }
}
