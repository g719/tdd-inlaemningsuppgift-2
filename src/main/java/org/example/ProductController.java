package org.example;

import com.google.gson.Gson;
import io.javalin.http.Context;
import org.eclipse.jetty.http.HttpStatus;

public class ProductController {

    private final ProductService productService;

    private final Gson gson = new Gson();

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    public void getProducts(Context context) {
        var todos = productService.getProducts();
        var json = gson.toJson(todos);
        context.status(HttpStatus.OK_200);
        context.result(json);
    }

    public void getProduct(Context context) {
        int id = Integer.parseInt(context.pathParam("id"));
        var product = productService.getProduct(id);
        if (product.isPresent()) {
            var json = gson.toJson(product.get());
            context.status(HttpStatus.OK_200);
            context.json(json);
        } else {
            context.status(HttpStatus.NOT_FOUND_404);
        }
    }
    public void addProduct(Context context) {

        var product = context.bodyAsClass(Product.class);

        var json = gson.toJson(productService.addProduct(product));
        context.status(HttpStatus.OK_200);
        context.result(json);

    }
    public void deleteProduct(Context context) {
        int id = Integer.parseInt(context.pathParam("id"));
        var product = productService.getProduct(id);
        if (product.isPresent()) {
            productService.delete(product.get().getId());
            context.status(HttpStatus.OK_200);
            context.result("deleted");
        } else {
            context.status(HttpStatus.NOT_FOUND_404);
        }
    }
    public void editProduct(Context context) {
        var product = context.bodyAsClass(Product.class);
        var oldProduct = productService.getProduct(product.getId());
        if (oldProduct.isPresent()) {
            var json = gson.toJson(productService.editProduct(product).orElseThrow());
            context.status(HttpStatus.OK_200);
            context.result(json);
        } else {
            context.status(HttpStatus.NOT_FOUND_404);
        }
    }
}
